#! env php
<?php

interface Documentation {
  public function getDocumentationFilename(string $sFormat) : string;

  public function toDocumentation(string $sDocumentationDir, string $sFormat = 'md') : string;
  public function writeDocumentation(string $sDocumentationDir, string $sFormat = 'md') : bool;
}

class Role implements Documentation {
  private string $sRoleName = '';
  private string $sRoleDirectory = '';
  private array  $arParamsList = [];

  private array $arTasks = [];
  private array $arHandlers = [];

  private bool  $bGalaxy = false;
  private array $arGalaxyParams = [];

  private static function showLog(string $sMessage) {
    echo '[' . date(DATE_ATOM) . '] ' . $sMessage . PHP_EOL;
  }

  public function __construct(string $sRoleName, string $sRoleDirectory = '', array $arParamsList = [], ?array $arGalaxyParams = null) {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sRoleName : '{$sRoleName}' - sRoleDirectory : '{$sRoleDirectory}' - arParamsList : " . implode(',', $arParamsList));
    $this->sRoleName = $sRoleName;
    $this->sRoleDirectory = $sRoleDirectory;
    $this->arParamsList = $arParamsList;

    if ($this->sRoleDirectory !== '') {
      $this->findTasks();
      $this->findHandlers();
    } elseif (is_array($arGalaxyParams)) {
      $this->bGalaxy = true;
      $this->arGalaxyParams = $arGalaxyParams;
    }
  }

  public function getName() : string {
    return $this->sRoleName;
  }

  public function setRoleDirectory(string $sRoleDirectory = '') : void {
    if ($this->sRoleDirectory === '') {
      $this->sRoleDirectory = $sRoleDirectory;
    }

    if ($this->sRoleDirectory !== '') {
      $this->arTasks = [];
      $this->arHandlers = [];
      $this->findTasks();
      $this->findHandlers();
    }
  }

  public function setParamsList(array $arParamsList = []) : void {
    if (count($arParamsList) > 0) {
      $this->arParamsList = $arParamsList;
    }
  }

  public function setGalaxyParams(?array $arGalaxyParams = null) : void {
    if (is_array($arGalaxyParams) && count($arGalaxyParams) > 0) {
      $this->bGalaxy = true;
      $this->arGalaxyParams = $arGalaxyParams;
    } else {
      $this->bGalaxy = false;
      $this->arGalaxyParams = [];
    }
  }

  private function findHandlers() : void {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - task search glob : '{$this->sRoleDirectory}/handlers/*'");
    foreach(glob($this->sRoleDirectory . '/handlers/*') as $sTaskFile) {
      $this->parseHandlersFile($sTaskFile);
    }
  }

  private function parseHandlersFile (string $sFilePath) : void {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilePath}'");
    $arYamlStruct = $this->readYamlFile($sFilePath);
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - arYamlStruct : " . print_r($arYamlStruct, true));

    if($arYamlStruct !== false) {

      foreach ($arYamlStruct as $iOrder => $arHandler) {
        $sHandlerName = ''; $sFunction = ''; $sRegisterVar = ''; $arFunctionParams = []; $arLoopParams = []; $arWhenParams = []; $arNotify = []; $arTags = [];

        foreach($arHandler as $sName => $mValue) {
          switch ($sName) {
            case 'name':
              $sHandlerName = (string)$mValue;
              break;
            case 'register':
              $sRegisterVar = (string)$mValue;
              break;
            case 'loop':
              $arLoopParams = $mValue;
              break;
            case 'when':
              $arWhenParams = $mValue;
              break;
            case 'notify':
              $arNotify = (is_array($mValue)) ? $mValue : [$mValue];
              break;
            case 'tags':
              $arTags = $mValue;
              break;
            default:
              $sFunction = (string)$sName;
              $arFunctionParams = (is_array($mValue)) ? $mValue : ['default' => $mValue];
              break;
          }
        }
        $this->arHandlers[$iOrder] = [
          'name' => $sHandlerName,
          'register' => $sRegisterVar,
          'function' => $sFunction,
          'function_params' => $arFunctionParams,
          'loop' => $arLoopParams,
          'when' => $arWhenParams,
          'notify' => $arNotify,
          'tags' => $arTags,
        ];
      }

      // print_r($this->arHandlers);
    }
  }

  private function findTasks() : void {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - task search glob : '{$this->sRoleDirectory}/tasks/*'");
    foreach(glob($this->sRoleDirectory . '/tasks/*') as $sTaskFile) {
      $this->parseTaskFile($sTaskFile);
    }
  }

  private function parseTaskFile (string $sFilePath) : void {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilePath}'");
    $arYamlStruct = $this->readYamlFile($sFilePath);
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - arYamlStruct : " . print_r($arYamlStruct, true));

    if($arYamlStruct !== false) {

      foreach ($arYamlStruct as $iOrder => $arTask) {
        $sTaskName = ''; $sFunction = ''; $sRegisterVar = ''; $arFunctionParams = []; $arLoopParams = []; $arWhenParams = []; $arNotify = []; $arTags = [];

        foreach($arTask as $sName => $mValue) {
          switch ($sName) {
            case 'name':
              $sTaskName = (string)$mValue;
              break;
            case 'register':
              $sRegisterVar = (string)$mValue;
              break;
            case 'loop':
              $arLoopParams = $mValue;
              break;
            case 'when':
              $arWhenParams = $mValue;
              break;
            case 'notify':
              $arNotify = (is_array($mValue)) ? $mValue : [$mValue];
              break;
            case 'tags':
              $arTags = $mValue;
              break;
            default:
              $sFunction = (string)$sName;
              $arFunctionParams = (is_array($mValue)) ? $mValue : ['default' => $mValue];
              break;
          }
        }
        $this->arTasks[$iOrder] = [
          'name' => $sTaskName,
          'register' => $sRegisterVar,
          'function' => $sFunction,
          'function_params' => $arFunctionParams,
          'loop' => $arLoopParams,
          'when' => $arWhenParams,
          'notify' => $arNotify,
          'tags' => $arTags,
        ];
      }

      // print_r($this->arTasks);
    }
  }

  private function readYamlFile (string $sFilePath) : array|false {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilePath}'");
    $arYamlStruct = false;

    if (file_exists($sFilePath)) {
      $arYamlStruct = yaml_parse_file($sFilePath);

    }
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - End - arYamlStruct : " . print_r($arYamlStruct, true));
    return $arYamlStruct;
  }

  public function getDocumentationFilename(string $sFormat) : string {
    return "role-{$this->sRoleName}.{$sFormat}";
  }

  private function generateTaskDocumentation(array $arTasks) : string {
    $sMDDoc = '';

    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - arTasks : " . print_r($arTasks, true));

    foreach($arTasks as $arTask) {
      $sMDDoc .= "\n### Tasks name : {$arTask['name']}\n" .
          "Function used here : [{$arTask['function']}](#)\n";

      if (count($arTask['function_params']) > 0) {
        $sMDDoc .= "\n**Functions' Parameters**\n";
        $sMDDoc .= "\n| name | value |\n| ----- | ----- |\n";

        foreach($arTask['function_params'] as $sName => $mValue) {
          if (is_array($mValue)) {
            $sValues = '';
            foreach($mValue as $sKey => $mVal) {
              $sVal = (is_array($mVal)) ? implode(', ', $mVal) : $mVal;
              $sValues .= "{$sKey} : `{$sVal}` <br /> ";
            }

            $sMDDoc .= "| {$sName} | {$sValues} |\n";
          } else {
            $sMDDoc .= "| {$sName} | `{$mValue}` |\n";
          }
        }
      }

      if (is_array($arTask['loop']) && count($arTask['loop']) > 0) {
        $sMDDoc .= "\n**Function loop on these values**\n";

        $arKeys = (is_array($arTask['loop'][0])) ? array_keys($arTask['loop'][0]) : ['item'];

        $sMDDoc .= "\n| index | " . implode(' | ', $arKeys) . " |\n";
        for ($i = 0; $i <= count($arKeys); $i ++) {
          $sMDDoc .= "| ----- ";
        }
        $sMDDoc .= "|\n";
        foreach($arTask['loop'] as $iIdx => $mItems) {
          $arItems = (is_array($mItems)) ? $mItems : [$mItems] ;
          $sMDDoc .= "| {$iIdx} | `" . implode('` | `', array_values($arItems)) . "` |\n";
        }

      } elseif (is_string($arTask['loop']) && $arTask['loop'] !== '') {
        $sMDDoc .= "**Function loop on these values**\n\nVariable : `{$arTask['loop']}`\n";
      }

      if ($arTask['register'] !== '') {
        $sMDDoc .= "\n**Function register return into variable** : `{$arTask['register']}`\n";
      }

      if (is_array($arTask['when']) && count($arTask['when']) > 0) {
        $sMDDoc .= "\n**Function executed only when**\n";
        foreach($arTask['when'] as $sValue) {
          $sMDDoc .= "* `{$sValue}` \n";
        }
      } elseif (is_string($arTask['when']) && $arTask['when'] !== '') {
        $sMDDoc .= "\n**Function  executed only when** : `{$arTask['when']}`\n";
      }

      if (count($arTask['notify']) > 0) {
        $sMDDoc .= "\n**Function send notification to handler**\n";
        foreach($arTask['notify'] as $sValue) {
          $sMDDoc .= "* `{$sValue}` \n";
        }
      }

      if (count($arTask['tags']) > 0) {
        $sMDDoc .= "\n**Associated to tags** : " . implode(', ', $arTask['tags']) . "\n";
      }
    }

    return $sMDDoc;
  }

  public function toDocumentation(string $sDocumentationDir, string $sFormat = 'md') : string {
    $sMDDoc = '';

    if ($this->bGalaxy) {
      $sMDDoc = <<< EOD
# Remote Role : {$this->sRoleName}

## Access to the remote role
|  |  |
| ----- | ----- |
| Method | `{$this->arGalaxyParams['scm']}` |
| Address | `{$this->arGalaxyParams['src']}` |
| Version | `{$this->arGalaxyParams['version']}` |
EOD;

      if (count($this->arParamsList) > 0) {
        $sMDDoc .= "## Used parameters : \n";
        foreach($this->arParamsList as $sParam) {
          $sMDDoc .= "* {$sParam} \n";
        }
      }
    } else {
      $sMDDoc = "# Role : {$this->sRoleName}\n" .
        "Based on directory : roles/{$this->sRoleName}\n\n";

      if (count($this->arParamsList) > 0) {
        $sMDDoc .= "Used parameters : \n";
        foreach($this->arParamsList as $sParam) {
          $sMDDoc .= "* {$sParam} \n";
        }
      }

      if (count($this->arTasks) > 0) {
        $sMDDoc .= "\n## Tasks of the role : \n\n";
        $sMDDoc .= $this->generateTaskDocumentation($this->arTasks);
      }

      if (count($this->arHandlers) > 0) {
        $sMDDoc .= "\n## Handlers of the role : \n\n";
        $sMDDoc .= $this->generateTaskDocumentation($this->arHandlers);
      }
    }

    return $sMDDoc;
  }

  public function writeDocumentation(string $sDocumentationDir, string $sFormat = 'md') : bool {
    $sMDDoc = $this->toDocumentation($sDocumentationDir, $sFormat);

    return (file_put_contents($sDocumentationDir . '/' . $this->getDocumentationFilename($sFormat), $sMDDoc) !== false);
  }
}

class Playbook implements Documentation {
  private ?Project $oProject = null;
  private string $sFileName = '';
  private string $sName = '';
  private string $sHosts = '';
  private array $arParams = [];
  private array $arRoles = [];
  private array $arTasks = [];

  private static function showLog(string $sMessage) {
    echo '[' . date(DATE_ATOM) . '] ' . $sMessage . PHP_EOL;
  }

  public function __construct(Project $oPrj, string $sFileName = '', string $sPathName = '') {
    $this->oProject = $oPrj;
    $this->sFileName = $sFileName;
    $this->sName = explode('.', $sFileName, -1)[0];

    $sFilePath = $sPathName . '/' . $sFileName;
    $this->parseYamlFile($sFilePath);
  }

  public function getName() : string {
    return $this->sName;
  }

  private function parseYamlFile (string $sFilePath) : void {
    //self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilePath}'");
    $arYamlStruct = $this->readYamlFile($sFilePath);

    if($arYamlStruct !== false) {
      foreach ($arYamlStruct[0] as $sKey => $mVal) {
        switch ($sKey) {
          case 'hosts':
            $this->sHosts = (string)$mVal;
            break;
          case 'roles':
            foreach ($mVal as $iOrder => $arRole) {
              $sRole = ''; $arTags = []; $arParams = [];

              foreach($arRole as $sName => $mValue) {
                switch ($sName) {
                  case 'role':
                    $sRole = $mValue;
                    break;
                  case 'tags':
                    $arTags = $mValue;
                    break;
                  default:
                    $arParams[$sName] = $mValue;
                    break;
                }
              }
              $this->arRoles[$iOrder] = ['role' => $sRole, 'tags' => $arTags, 'params' => $arParams];


              $this->oProject->addRole($sRole, '', array_keys($arParams));
            }
            break;
          case 'tasks':
            foreach ($mVal as $iOrder => $arTask) {
              $sTaskName = ''; $sFunction = ''; $sRegisterVar = ''; $arFunctionParams = []; $arLoopParams = []; $arWhenParams = []; $arNotify = []; $arTags = [];

              foreach($arTask as $sName => $mValue) {
                switch ($sName) {
                  case 'name':
                    $sTaskName = (string)$mValue;
                    break;
                  case 'register':
                    $sRegisterVar = (string)$mValue;
                    break;
                  case 'loop':
                    $arLoopParams = $mValue;
                    break;
                  case 'when':
                    $arWhenParams = $mValue;
                    break;
                  case 'notify':
                    $arNotify = (is_array($mValue)) ? $mValue : [$mValue];
                    break;
                  case 'tags':
                    $arTags = $mValue;
                    break;
                  default:
                    $sFunction = (string)$sName;
                    $arFunctionParams = (is_array($mValue)) ? $mValue : ['default' => $mValue];
                    break;
                }
              }
              $this->arTasks[$iOrder] = [
                'name' => $sTaskName,
                'register' => $sRegisterVar,
                'function' => $sFunction,
                'function_params' => $arFunctionParams,
                'loop' => $arLoopParams,
                'when' => $arWhenParams,
                'notify' => $arNotify,
                'tags' => $arTags,
              ];
            }
            break;
          default:
            $this->arParams[$sKey] = $mVal;
            break;
        }
      }
    }

    //self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - End");
  }

  private function readYamlFile (string $sFilePath) : array|false {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilePath}'");
    $arYamlStruct = false;

    if (file_exists($sFilePath)) {
      $arYamlStruct = yaml_parse_file($sFilePath);

    }
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - End - arYamlStruct : " . print_r($arYamlStruct, true));
    return $arYamlStruct;
  }

  public function getDocumentationFilename(string $sFormat) : string {
    return "playbook-{$this->sName}.{$sFormat}";
  }

  private function generateTaskDocumentation(array $arTasks) : string {
    $sMDDoc = '';

    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - arTasks : " . print_r($arTasks, true));

    foreach($arTasks as $arTask) {
      $sMDDoc .= "\n### Tasks name : {$arTask['name']}\n" .
          "Function used here : [{$arTask['function']}](#)\n";

      if (count($arTask['function_params']) > 0) {
        $sMDDoc .= "\n**Functions' Parameters**\n";
        $sMDDoc .= "\n| name | value |\n| ----- | ----- |\n";

        foreach($arTask['function_params'] as $sName => $mValue) {
          if (is_array($mValue)) {
            $sValues = '';
            foreach($mValue as $sKey => $mVal) {
              $sVal = (is_array($mVal)) ? implode(', ', $mVal) : $mVal;
              $sValues .= "{$sKey} : `{$sVal}` <br /> ";
            }

            $sMDDoc .= "| {$sName} | {$sValues} |\n";
          } else {
            $sMDDoc .= "| {$sName} | `{$mValue}` |\n";
          }
        }
      }

      if (is_array($arTask['loop']) && count($arTask['loop']) > 0) {
        $sMDDoc .= "\n**Function loop on these values**\n";

        $arKeys = (is_array($arTask['loop'][0])) ? array_keys($arTask['loop'][0]) : ['item'];

        $sMDDoc .= "\n| index | " . implode(' | ', $arKeys) . " |\n";
        for ($i = 0; $i <= count($arKeys); $i ++) {
          $sMDDoc .= "| ----- ";
        }
        $sMDDoc .= "|\n";
        foreach($arTask['loop'] as $iIdx => $mItems) {
          $arItems = (is_array($mItems)) ? $mItems : [$mItems] ;
          $sMDDoc .= "| {$iIdx} | `" . implode('` | `', array_values($arItems)) . "` |\n";
        }

      } elseif (is_string($arTask['loop']) && $arTask['loop'] !== '') {
        $sMDDoc .= "**Function loop on these values**\n\nVariable : `{$arTask['loop']}`\n";
      }

      if ($arTask['register'] !== '') {
        $sMDDoc .= "\n**Function register return into variable** : `{$arTask['register']}`\n";
      }

      if (is_array($arTask['when']) && count($arTask['when']) > 0) {
        $sMDDoc .= "\n**Function executed only when**\n";
        foreach($arTask['when'] as $sValue) {
          $sMDDoc .= "* `{$sValue}` \n";
        }
      } elseif (is_string($arTask['when']) && $arTask['when'] !== '') {
        $sMDDoc .= "\n**Function  executed only when** : `{$arTask['when']}`\n";
      }

      if (count($arTask['notify']) > 0) {
        $sMDDoc .= "\n**Function send notification to handler**\n";
        foreach($arTask['notify'] as $sValue) {
          $sMDDoc .= "* `{$sValue}` \n";
        }
      }

      if (count($arTask['tags']) > 0) {
        $sMDDoc .= "\n**Associated to tags** : " . implode(', ', $arTask['tags']) . "\n";
      }
    }

    return $sMDDoc;
  }

  public function toDocumentation(string $sDocumentationDir, string $sFormat = 'md') : string {
    $sMDDoc = "# Playbook : {$this->sFileName}\n\nBased on hosts : {$this->sHosts}\n";

    if (count($this->arRoles) > 0) {
      $sMDDoc .= "\n## Roles of the playbook : \n\n";
      foreach($this->arRoles as $arRole) {
        $sMDDoc .= "### Role : {$arRole['role']}\n" .
            "Detailed info here : [Role : {$arRole['role']}](role-{$arRole['role']}.{$sFormat})\n";

        if (count($arRole['tags']) > 0) {
          $sMDDoc .= "\n**Associated to tags** : " . implode(', ', $arRole['tags']) . "\n";
        }

        if (count($arRole['params']) > 0) {
          $sMDDoc .= "\n**Role params**\n";
          $sMDDoc .= "\n| name | value |\n| ----- | ----- |\n";

          foreach($arRole['params'] as $sName => $sValue) {
            $sMDDoc .= "| {$sName} | `{$sValue}` |\n";
          }
        }

      }
    }

    if (count($this->arTasks) > 0) {
      $sMDDoc .= "\n## Tasks of the playbook : \n\n";
      $sMDDoc .= $this->generateTaskDocumentation($this->arTasks);
    }

    return $sMDDoc;
  }

  public function writeDocumentation(string $sDocumentationDir, string $sFormat = 'md') : bool {
    $sMDDoc = $this->toDocumentation($sDocumentationDir, $sFormat);

    return (file_put_contents($sDocumentationDir . '/' . $this->getDocumentationFilename($sFormat), $sMDDoc) !== false);
  }
}

class Project implements Documentation {
  private string $sPathName = '';
  private string $sName = '';
  private array $arConfig = [];
  private array $arInventory = [];
  private array $arPlaybooks = [];
  private array $arRoles = [];

  private static function showLog(string $sMessage) {
    echo '[' . date(DATE_ATOM) . '] ' . $sMessage . PHP_EOL;
  }

  public function __construct(string $sPathName = '') {
    $this->sPathName = $sPathName;
    $this->sName = basename($sPathName);

    $this->findConfig();
    $this->findInventory();
    $this->findPlaybook();
    $this->findGalaxyRoles();
    $this->findLocalRoles();
  }

  public function getName() : string {
    return $this->sName;
  }

  public function addRole(string $sRoleName, string $sRoleDir, array $arParamsList, ?array $arGalaxyRole = null) : void {
    if (array_key_exists($sRoleName, $this->arRoles)) {
      $this->arRoles[$sRoleName]->setRoleDirectory($sRoleDir);
      $this->arRoles[$sRoleName]->setParamsList($arParamsList);
      $this->arRoles[$sRoleName]->setGalaxyParams($arGalaxyRole);
    } else {
      $this->arRoles[$sRoleName] = new Role($sRoleName, $sRoleDir, $arParamsList, $arGalaxyRole);
    }

    //return $this->arRoles[$sRoleName];
  }

  private function findConfig() : void {
    $sFilename = $this->sPathName . '/ansible.cfg';
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilename}'");
    if (file_exists($sFilename)) {
      $arCfgStruct = parse_ini_file($sFilename, true);
      // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - arCfgStruct : " . print_r($arCfgStruct, true));

      if($arCfgStruct !== false) {
        $this->arConfig = $arCfgStruct;
      }
    }
  }

  private function inventoryParser (string $sFilename) : ?array {
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilename}'");
    if (file_exists($sFilename)) {
      $arLines = file($sFilename);
      if (is_array($arLines) && count($arLines) > 0 && trim($arLines[0]) === '---') {
        $arYamlStruct = yaml_parse_file($sFilename);

        if($arYamlStruct !== false) {
          // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - YAML - arYamlStruct : " . print_r($arYamlStruct, true));
          return $arYamlStruct;
        }

        return null;
      } elseif (is_array($arLines) && count($arLines) > 0) {
        // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - INI - arLines : " . print_r($arLines, true));
        $arStruct = []; $sSection = 'default';

        foreach ($arLines as $sLine) {
          $sLine = trim($sLine);
          if (preg_match('/\[([a-zA-Z0-9_-]+)\]/', $sLine, $arMatchs)) {
            $sSection = $arMatchs[1];
          } else {
            $sHost = ''; $arHostParams = [];
            $arParsed = explode(' ', $sLine);
            foreach($arParsed as $sParam) {
              $sParam = trim($sParam);
              if ($sParam !== '') {
                if ($sHost === '') {
                  $sHost = $sParam;
                } else {
                  list($sKey, $sValue) = explode('=', $sParam, 2);
                  $arHostParams[$sKey] = $sValue;
                }
              }
            }
            $arStruct[$sSection][$sHost] = $arHostParams;
          }
        }

        return $arStruct;
      }
    }

    return null;
  }

  private function findInventory() : void {
    $sFilename = (isset($this->arConfig['defaults']['inventory'])) ? $this->sPathName . '/' . $this->arConfig['defaults']['inventory'] : '/etc/ansible/hosts';

    $arStruct = $this->inventoryParser($sFilename);
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - arStruct : " . print_r($arStruct, true));

    if($arStruct !== null) {
      $this->arInventory = $arStruct;
    }
  }

  private function findPlaybook() : void {
    foreach(glob($this->sPathName . '/*.yml') as $sPlaybookFullFileName) {
      $sFileName = basename($sPlaybookFullFileName);
      $oPlaybook = new Playbook($this, $sFileName, $this->sPathName);

      $this->arPlaybooks[] = $oPlaybook;
    }
  }

  private function findGalaxyRoles() : void {
    $sFilename = $this->sPathName . '/roles/requirements.yml';
    // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - Start - sFilePath : '{$sFilename}'");
    if (file_exists($sFilename)) {
      $arYamlStruct = yaml_parse_file($sFilename);
      // self::showLog(__CLASS__ . '::' . __FUNCTION__ . " - arYamlStruct : " . print_r($arYamlStruct, true));

      if($arYamlStruct !== false) {
        foreach($arYamlStruct as $arGalaxyRole) {
          $this->addRole($arGalaxyRole['name'], '', [], $arGalaxyRole);
        }
      }
    }
  }

  private function findLocalRoles() : void {
    foreach(glob($this->sPathName . '/roles/*', GLOB_ONLYDIR) as $sDirRole) {
      $sRoleName = basename($sDirRole);
      $this->addRole($sRoleName, $sDirRole, []);
    }
  }

  public function getDocumentationFilename(string $sFormat) : string {
    return "index.{$sFormat}";
  }

  public function toDocumentation(string $sDocumentationDir, string $sFormat = 'md') : string {
    $sMDDoc = "## Projet : {$this->sName}\n";

    if (count($this->arPlaybooks) > 0) {
      $sMDDoc .= "\n## List of playbooks : \n\n";
      foreach($this->arPlaybooks as $oPlaybook) {
        $sPlayBookName = $oPlaybook->getName();
        $sDocFileName = $oPlaybook->getDocumentationFilename($sFormat);
        $sMDDoc .= "* Playbook [{$sPlayBookName }]({$sDocFileName})\n";
      }
    }

    if (count($this->arInventory) > 0) {
      $sMDDoc .= "\n## Ansible static inventory : \n";

      foreach($this->arInventory as $sGroupName => $arHostGroup) {
        $sMDDoc .= "\n**Group {$sGroupName}**\n\n| Host | Parameters |\n| ----- | ----- |\n";

        foreach($arHostGroup as $sHostName => $mValue) {
          if (is_array($mValue)) {
            $sValues = '';
            foreach($mValue as $sKey => $mVal) {
              $sVal = (is_array($mVal)) ? implode(', ', $mVal) : $mVal;
              $sValues .= "{$sKey} : `{$sVal}` <br /> ";
            }

            $sMDDoc .= "| {$sHostName} | {$sValues} |\n";
          } else {
            $sMDDoc .= "| {$sHostName} | None |\n";
          }
        }
      }
    }

    if (count($this->arConfig) > 0) {
      $sMDDoc .= "\n## Ansible configuration : \n";

      foreach($this->arConfig as $sGroupName => $arGroupConfig) {
        $sMDDoc .= "\n**Group {$sGroupName}**\n\n| Name | Value |\n| ----- | ----- |\n";

        foreach($arGroupConfig as $sName => $mValue) {
          if (is_array($mValue)) {
            $sValues = '';
            foreach($mValue as $sKey => $mVal) {
              $sVal = (is_array($mVal)) ? implode(', ', $mVal) : $mVal;
              $sValues .= "{$sKey} : `{$sVal}` <br /> ";
            }

            $sMDDoc .= "| {$sName} | {$sValues} |\n";
          } else {
            $sMDDoc .= "| {$sName} | `{$mValue}` |\n";
          }
        }
      }
    }


    return $sMDDoc;
  }

  public function writeDocumentation(string $sDocumentationDir = "docs", string $sFormat = 'md') : bool {
    $sMDDoc = $this->toDocumentation($sDocumentationDir, $sFormat);

    foreach($this->arPlaybooks as $oPlaybook) {
      $oPlaybook->writeDocumentation($sDocumentationDir, $sFormat);
    }

    foreach($this->arRoles as $oRole) {
      $oRole->writeDocumentation($sDocumentationDir, $sFormat);
    }

    return (file_put_contents($sDocumentationDir . '/' . $this->getDocumentationFilename($sFormat), $sMDDoc) !== false);
  }
}


class CliParam {
  private $arActivatedFunction = [
    'export_dir'   => 'docs',
    'playbook_dir' => '',
    'format'   => 'md',
    'help'     => false,
  ];

  private $arArguments = [];

  private static function showLog(string $sMessage) {
    echo '[' . date(DATE_ATOM) . '] ' . $sMessage . PHP_EOL;
  }

  public function __construct(array $arArgv = [])  {
    $this->arArguments = $arArgv;
    array_shift($this->arArguments);
  }

  public function readParams(): void {
    $this->arActivatedFunction['help'] = false;
    foreach ($this->arArguments as $sParam) {
      switch ($sParam) {
        case '--help':
          $this->arActivatedFunction['help'] = true;
          break;
        case '--md':
          $this->arActivatedFunction['format'] = 'md';
          break;
          default:
          if (substr($sParam, 0, 2) == '--') {
            self::showLog("Error, Bad parametter '{$sParam}' ...");
            exit(1);
          }

          if ($this->arActivatedFunction['playbook_dir'] === '') {
            $this->arActivatedFunction['playbook_dir'] = $sParam;
          }
          break;
      };
    }
  }

  public function showHelp() : void {
    echo <<< EOD
  php ansible_docs.php [options] playbook_path

  options :
    --help        show this help page
    --md          generate md formated doc
EOD;
  }

  public function __set(string $sName, mixed $mValue): void {
    self::showLog("__set('{$sName}', '{$mValue}')");
    // Don't modify
  }

  public function __get(string $sName): mixed {
    if (isset($this->arActivatedFunction[$sName])) {
      return $this->arActivatedFunction[$sName];
    } else {
      return null;
    }
  }

  public function __isset(string $sName): bool {
    return isset($this->arActivatedFunction[$sName]);
  }

  public function __unset(string $sName): void {
    // Don't modify
  }
}

$oCliParam = new CliParam($argv);
$oCliParam->readParams();
if ($oCliParam->help) {
  $oCliParam->showHelp();
  exit(0);
}

if ($oCliParam->playbook_dir === '') {
  echo "You didn't give a playbook path ...";
  exit(1);
}

if (!is_dir($oCliParam->export_dir)) {
  mkdir($oCliParam->export_dir, 0755, true);
}

$oProject = new Project($oCliParam->playbook_dir);
$oProject->writeDocumentation($oCliParam->export_dir, $oCliParam->format);

unset($oCliParam, $oProject);
exit(0);

