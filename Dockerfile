FROM php:8.2-cli

RUN groupadd --gid 1000 debian
RUN useradd --uid 1000 --gid debian --shell /bin/bash --create-home debian
RUN usermod -a -G www-data debian

RUN apt update -y && apt upgrade -y && apt install -y curl git

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions @composer bz2 curl gettext intl mcrypt opcache xsl yaml zip

ADD docker/php/ansible-docs.ini /usr/local/etc/php/conf.d/ansible-docs.ini

# Make Proper
RUN apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    rm -rf /var/lib/apt/lists/*;

RUN mkdir -p /var/www/app && chown debian. /var/www/app
WORKDIR /var/www/app
USER debian

ADD ansible-docs.php /var/www/ansible-docs.php 

CMD php /var/www/ansible-docs.php /var/www/app

